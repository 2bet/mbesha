const { Pool } = require('pg')

const settings = {
  host: process.env.PG_HOST,
  database: process.env.PG_DATABASE,
  port: process.env.PG_PORT,
  user: process.env.PG_USER,
  password: process.env.PG_PASSWORD
};

const pool = new Pool(settings);
const security_credential=process.env.MASTER_KEY;
const consumer_key=process.env.CONSUMER_KEY;
const consumer_secret=process.env.CONSUMER_SECRET;
const online_pass_key=process.env.PASS_KEY;
const short_code=process.env.SHORT_CODE;
const initiator_name = process.env.INITIATOR_NAME;
// const online_short_code=174379;
// const online_pass_key="bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919";
// const security_credential="Safaricom007@";

module.exports = {
  pool,
  //master_key,
  consumer_key,
  consumer_secret,
  // pass_key,
  short_code,
  initiator_name,
  // online_short_code,
  online_pass_key,
  security_credential,
  knex: require('knex')({
    client: 'pg',
    connection: settings
  })
}
