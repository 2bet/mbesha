require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const { knex } = require('./config/configs');
const util = require('./util');
const mpesa = require('./mpesa');

const app = express();


app.use(bodyParser.json({ limit: '30mb' }));
app.use(bodyParser.text({ type: 'text/*' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


const allowedOrigins = ['http://localhost:3038', 'https://api.safaricom.co.ke'];

app.use(cors({
  origin: function(origin, callback){
    // allow requests with no origin 
    // (like mobile apps or curl requests)
    if(!origin) {
      return callback(null, true);

    }
    if(allowedOrigins.indexOf(origin) === -1){
      let errMsg = 'CORS policy does not allow your origin to make requests here!';
      return callback(new Error(errMsg), false);

    }
    return callback(null, true);

  },
  allowedHeaders: ['Content-Type', 'Authorization', 'Origin', 'X-Requested-With', 'Accept,token']

}));


app.post('/topup/mpesa', async function (req, res) {
    try {
        let resp = await mpesa.sendMpesa(req);
        console.log("===========================Send Mpesa================================");
        console.log(resp);
        console.log("===========================Send Mpesa================================");

        res.status(200).json({
            statuscode: 0,
            statusname: "Success",
            Successmessage: 'Mpesa Success',
            Message:resp
        });

    } catch(err) {
        console.log(err);

        res.status(500).json({
            statuscode: 1,
            statusname: "Error",
            Successmessage: 'Mpesa Error',
            Error: err
        });

    }

});


app.post('/withdraw/mpesa', async function (req, res) {

    try {
        let resp = await mpesa.withdrawMpesa(req);
        console.log("===========================Withdraw Mpesa================================");
        console.log(resp);
        console.log("===========================Withdraw Mpesa================================");

        res.status(200).json({
            statuscode: 0,
            statusname: "Success",
            Successmessage: 'Mpesa Success',
            Message:resp
        });

    } catch(err) {
        console.log(err);

        res.status(500).json({
            statuscode: 1,
            statusname: "Error",
            Successmessage: 'Mpesa Error',
            Error: err
        });

    }

});


app.post('/lipanampesa/callback', mpesa.mpesaCallback);


app.post('/b2c/callback', mpesa.b2cCallback);


app.get('/', function (req, res){
  res.send('Mbesha server is up and Running');
});


app.post('/get_token', async function (req, res) {
  try {

    const tkn_data = {
      service: 'Mbesha'
    };

    var token = await util.security.encodeToken(tkn_data);

    res.status(200).json({
      statuscode: 0,
      statusname: "Success",
      Token: token
    });
  } catch (err) {
    res.status(500).json({
      statuscode: 1,
      statusname: "Error",
      Error: err
    });
  }
});

const port = 9290;
app.listen(port, () => console.log(`Mbesha listening on port ${port}!`))