var jwt = require('jsonwebtoken');

module.exports.encodeToken = data => new Promise(
  (resolve, reject) => {
    try {
      resolve(
        jwt.sign({
            data
          },
          '2bet2020*', {
            expiresIn: '1h'
          }
        ))
    } catch (err) {
      reject(err)
    }
  }
)

module.exports.decodeToken = req => new Promise(
  (resolve, reject) => {
    try {
      console.log("verifying token");
      console.log(req);
      console.log("verifying token");
      var token = req.body.token || req.headers['token'] || req.session.token.token;

      if (token || token !== undefined) {
        var decoded = jwt.verify(
          token.toString(),
          '2bet2020*', {
            algorithms: ['HS256']
          }
        );
        resolve(decoded)
      } else {
        reject('Access token is required ');
      }
    } catch (err) {
      // err
      console.log(err);
      reject(err);
    }
  }
)