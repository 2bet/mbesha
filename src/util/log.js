const winston = require('winston');
require('winston-daily-rotate-file');
const fs = require('fs');

const logDir = 'logs';

if (!fs.existsSync(logDir)) {
	fs.mkdirSync(logDir);
}

const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = winston.createLogger({
	transports: [
		// colorize the output to the console
		new(winston.transports.Console)({
			timestamp: tsFormat,
			colorize: true,
			level: 'error',
			prettyPrint: true
		}),
		new(winston.transports.DailyRotateFile)({
			filename: `${logDir}/mbesha.log`,
			timestamp: tsFormat,
			datePattern: 'YYYY-MM-DD',
			zippedArchive: true,
			prepend: true,
			//handleExceptions: true,
			prettyPrint: true,
			level: 'info'
		})
	]
});

module.exports.log_info = async  data => {
	logger.log('info', data);
}

module.exports.log_error = async data => {
	logger.log('error', data);
}