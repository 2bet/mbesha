const Mpesa = require('mpesa-node');
const path = require('path');
const{
    consumer_key,
    consumer_secret,
    // pass_key,
    // master_key,
    short_code,
    initiator_name,
    // online_short_code,
    online_pass_key,
    security_credential
} = require('../config/configs');


const cert = path.resolve('./src/mpesa/keys/cert.cer');
// const msisdn = 254708374149;
const URL = "https://mpesa.2bet.co.ke"; //TODO: modify callback URL here as deemed appropriate


const mpesa = new Mpesa({
    consumerKey: consumer_key,
    consumerSecret: consumer_secret,
    environment: 'production',
    shortCode: short_code,
    initiatorName: initiator_name,
    lipaNaMpesaShortCode: short_code,
    lipaNaMpesaShortPass: online_pass_key,
    securityCredential: security_credential,
    certPath: cert
});

/*
* B2C customer transaction, withdrawal transacions made by the customer
* @param: {number} senderParty/shortCode-  the initator of the transaction; organization
* @param: {number} receiverParty/phone- the party receiving the payment; customer
* @param: {number} amount - the amuont to be transacted (kenyan shillings)
* @param: {string} queUrl  - the url to handle timout requests
* @param: {string} resultUrl  - the callback url to handle responses
* @param: {string} commandId  - the current transaction name given
* @param: {string} initiatorName  - the name of the transaction initiator in the organization
* @param: {string} remarks  - any remarks given to the transaction
* @param: {string} occasion  - the occation of the transaction; the default (null)
*
*/
const B2CTransact = async (
    phone,
    transactionAmount,
) => {
    mpesa.b2c(
        // TODO: replace msisdn and amount with function parameters
        senderParty = short_code,
        receiverParty = phone,
        amount = transactionAmount,
        queueUrl = URL + '/b2c/callback',
        resultUrl = URL + '/b2c/callback'
    )
    /**
     * receiverParty = phone,
     * amount = transactionAmount,
     * queueUrl = callback+'/callback/mpesa/v1',
     * resultUrl = callback+'/callback/mpesa/v1'
     */
        .then((result) => {
            console.log(result);

        })
        .catch((err) => {
            console.log(err);

        })

};


/**
 * lipa na mpesa online checkout transaction
 * @param: senderMsisdn int - the customer making a deposit to their account
 * @param: amount int - the amount to be transacted
 * @param: callbackUrl string - the path to handle the response
 * @param: accountRef string - the sample account identifier
 * @param: transactionalDesc string - custom transaction description
 * @param: transactionType string - custom transaction type e.g. credit
 * @param: shortCode int - the organization paybill receiving the paymnet
 * @param: passKey string - assigned lipa na mpesa online passkey
 *
 */
const expressCheckout = async (
    phone,
    transactionAmount
) => {
    let accRef = Math.random().toString(35).substr(2, 7);
    mpesa.lipaNaMpesaOnline(
        senderMsisdn = phone,
        amount = transactionAmount,
        callbackUrl = URL + '/lipanampesa/callback',
        accountRef = accRef
    )
        .then((result) => {
            console.log(result.data);

        })
        .catch((err) => {
            console.log(err.response.data);

        })
};


module.exports = {
    B2CTransact,
    expressCheckout
};
