const moment = require('moment');
const mpesa = require('./mpesa');
const { knex } = require('../config/configs');
const util = require('../util');



const sendMpesa = async (req) => {
    util.log.log_info(req.body);
    const { phone, amount } = req.body;
    const contact = formatNumber(phone);

    try {
        await mpesa.expressCheckout(
            contact,
            amount
        )

    } catch(err) {
        util.log.log_error(err);
        console.log(err);

    }

};


const withdrawMpesa = async(req) => {
    util.log.log_info(req.body);

    const {phone, amount} = req.body;
    const contact = formatNumber(phone);

    try {
        await mpesa.B2CTransact(
            contact,
            amount
        )

    } catch(err) {
        util.log.log_error(err);
        console.log(err);

    }

};


const mpesaCallback = async (req, res) => {
    try {
        util.log.log_info(req.body);
        const realIP = (req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress).split(",")[0];

        if (realIP) {
            req.clientIP = realIP;

        } else {
            req.clientIP = req.socket.remoteAddress;

        }

        console.log("===========================Mpesa Checkout Callback================================");
        console.log("IP Address: "+ req.clientIP);
        console.log(req.body);

        if (req.body.Body.stkCallback.ResultCode === 0) {
            console.log('Callback Successful');

            // store transaction
            const ctx = req.body.Body.stkCallback.CallbackMetadata.Item;
            const phone = ctx.pop().Value.toString();
            const amount = ctx[0].Value;


            const acc_no = await knex('private.account').where('code', phone).select('id').first();
            const journal_no = await knex('private.journal').where('name', 'Deposit').select('id').first();

            const ctxEntry = {
                account_id: acc_no.id,
                journal_id: journal_no.id,
                credit: amount
            };

            const inserted_data = await knex('private.posting').insert(ctxEntry);

            // TODO:send sms to user upon failure

            res.status(200).json({
                statuscode: 0,
                statusname: "Success",
                Successmessage: 'Transaction done'
            });

        } else {
            res.status(500).json({
                statuscode: 1,
                statusname: "Error",
                Errormessage: req.body.Body.ResponseDescription
            });

        }

    } catch (error) {
        console.log(error);
        util.log.log_error(error);

        res.status(500).json({
            statuscode: 1,
            statusname: "Error",
            Errormessage: error
        });

    }

    console.log("===========================Mpesa Checkout Callback================================");

};


const b2cCallback = async (req, res) => {
    try {
        util.log.log_info(req.body);
        const realIP = (req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress).split(",")[0];

        if (realIP) {
            req.clientIP = realIP;

        } else {
            req.clientIP = req.socket.remoteAddress;

        }

        console.log("===========================Mpesa B2C Callback================================");
        console.log("IP Address: "+ req.clientIP);
        console.log(req.body);

        if (req.body.Result.ResultCode === 0) {
            console.log('Callback Successful');

            // store transaction
            const ctx = req.body.Body.stkCallback.CallbackMetadata.Item;
            const phone = ctx.pop().Value.toString();
            const amount = ctx[0].Value;

            const acc_no = await knex('private.account').where('code', phone).select('id').first();
            const journal_no = await knex('private.journal').where('name', 'Withdrawal').select('id').first();

            const ctxEntry = {
                account_id: acc_no.id,
                journal_id: journal_no.id,
                credit: amount
            };

            const inserted_data = await knex('private.posting').insert(ctxEntry);

            res.status(200).json({
                statuscode: 0,
                statusname: "Success",
                Successmessage: 'Transaction done'
            });

        } else {
            res.status(500).json({
                statuscode: 1,
                statusname: "Error",
                Errormessage: req.body.Body.ResponseDescription
            });

        }

    } catch (error) {
        console.log(error);
        util.log.log_error(error);

        res.status(500).json({
            statuscode: 1,
            statusname: "Error",
            Errormessage: error
        });

    }
    console.log("===========================Mpesa B2C Callback================================");

};


function formatNumber(contact) {
    if (contact.startsWith("0")) {
        return "254" + contact.substr(1);
    }

    if (contact.startsWith("254")) {
        return contact
    }
}



module.exports = {
    sendMpesa,
    withdrawMpesa,
    mpesaCallback,
    b2cCallback
};
